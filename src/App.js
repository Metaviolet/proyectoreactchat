import React, { Component } from 'react';
import Post from './Post/component/Post';
import { Button, CardPanel, Row, Col, Input } from 'react-materialize';
import 'materialize-css/dist/css/materialize.min.css';
import 'materialize-css/dist/js/materialize.min.js';
import firebase from 'firebase/app';
import 'firebase/database';
import './App.css';

class App extends Component {
  constructor(props) {
     super(props); 
     this.state = { 
       posts : [],
       newPostBody: '', 
       loggedUser: '',
       loggedIn: false,
      }
      // config copiado de firebase ya que hemos creado ahi el proyecto
      const config = {
        apiKey: "AIzaSyCH756hpMcAlTFWd3PABB_7WmPhqYKwbqE",
        authDomain: "reactchatproject.firebaseapp.com",
        databaseURL: "https://reactchatproject.firebaseio.com",
        projectId: "reactchatproject",
        storageBucket: "reactchatproject.appspot.com",
        messagingSenderId: "993963248796"
      }
    // referencia al objeto de firebase
        this.app = firebase.initializeApp(config);
    //referencia a la base de datos
        this.database = this.app.database();
    // referencia a la "tabla" post
      this.databaseRef = this.database.ref().child('post'); 
    } 
    componentWillMount() {
      this.databaseRef.on('child_added', snapshot => {
         const response = snapshot.val();
         this.updateLocalState(response);
      });
    }
    addPost = (newPostBody, loggedUser) => { 
     // copiar el estado
      const postToSave = { name: this.state.loggedUser, message: this.state.newPostBody};
     // guardar en firebase nuestros posts
     this.databaseRef.push().set(postToSave);
     this.setState({ newPostBody: '' })
    } 
    handlePostEditorInputChange = (e) => {
      this.setState({
        newPostBody: e.target.value
      });

     } 
    handleUserNameInputChange = (e) => { 
      this.setState({
        loggedUser: e.target.value
      });
    }
    //Login Functions
    handleLogin = () => {
      this.setState({ loggedIn: true });
    }
    handleLogout = () => {
      this.setState({ loggedIn: false, loggedUser: '', newPostBody: ''  })
    }
    //Saving to firebase Functions
    updateLocalState = (response) => {
      // copia del estado actual
       const posts = this.state.posts;
       // actualizar la copia del estado
     posts.push(response);
       // actualizar el estado
      posts.reverse();
      this.setState(posts);
     } 
  render() {
    return (
      <Row className="mainContainer">
      <h4>Say it with React </h4>     
        
          <Col s={12}>
            <CardPanel className="lighten-4 black-text messagesBox">
            {this.state.posts.map((item, idx) => { 
              return (
                <Post key={idx} userName={item.name} postBody={item.message} />
              ) 
              }) }
            </CardPanel>

          <CardPanel className="lighten-4 black-text loginBox" data-isVisible={this.state.loggedIn}>
            <h5>Haz login para empezar a chatear!</h5>
            <Input onChange={this.handleUserNameInputChange} s={12} label="Tu nombre" placeholder="Mr.Stranger" value={this.state.loggedUser} />
            <Button onClick={this.handleLogin} waves='light' node='a' large={true}> Entrar</Button>
          </CardPanel>

          <CardPanel className="lighten-4 black-text messageInput" data-isVisible={this.state.loggedIn}>
            <h5> Hola {this.state.loggedUser}!!</h5>
            <Input onChange={this.handlePostEditorInputChange} label="Escribe tu mensaje" placeholder="Say something" s={12} value={this.state.newPostBody}/>
            <Button className="sendBtn" onClick={this.addPost}  waves='light' node='a' large={true}> Enviar</Button>
            <Button onClick={this.handleLogout} waves='light' node='a' large={true}>Logout</Button>
          </CardPanel>
        </Col>
    </Row>
    );
  }
}

export default App;
